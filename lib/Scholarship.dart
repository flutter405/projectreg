import 'package:flutter/material.dart';

class Scholarship extends StatefulWidget {
  // const Scholarship({super.key});
  var orientation, size, height, width;

  @override
  State<Scholarship> createState() => _Scholarship();
}

class _Scholarship extends State<Scholarship> {
  @override
  Widget build(BuildContext context) {
    final orientation = MediaQuery.of(context).orientation;
    final size = MediaQuery.of(context).size;
    final height = size.height;
    final width = size.width;

    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: buildAppBar(),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: NetworkImage(
                "https://scontent.fbkk5-7.fna.fbcdn.net/v/t1.15752-9/281152851_5910050635677276_1323122901884701339_n.png?_nc_cat=107&ccb=1-7&_nc_sid=ae9488&_nc_ohc=H4IUTTAn07AAX88N3aL&_nc_ht=scontent.fbkk5-7.fna&oh=03_AdQVJk8npvzUH1leIAcSFeIDo-AkkTGmuB-k64Y_wKFhvQ&oe=63F49539"),
            fit: BoxFit.cover,
          ),
        ),
        padding: EdgeInsets.all(15.0),
        child: ListView(
          children: <Widget>[
            TablePayment(width),
            Divider(height: 20),
            CardTotal(),
          ],
        ),
      ),
    );
  }

  AppBar buildAppBar() {
    return AppBar(
      backgroundColor: Colors.amberAccent.shade400,
      title: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            "SCHOLARSHIP",
            style: TextStyle(color: Colors.black),
          )
        ],
      ),
    );
  }
}

Widget TablePayment(width) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.center,
    children: <Widget>[
      Row(
        children: <Widget>[
          Column(
            children: [
              Text(
                'Debt : 2/2022',
                style: TextStyle(fontSize: 17),
              )
            ],
          ),
          Column(
            children: [
             Icon(Icons.arrow_drop_down,size: 30,)
            ],
          ),
        ],
      ),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          DataTable(columns: [
            DataColumn(
                label: Text('List',
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 13))),
            DataColumn(
                label: Text('Amount',
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 13))),
          ], rows: [
            DataRow(cells: [
              DataCell(Center(
                child: (Text('Course Registration fee',
                    textAlign: TextAlign.start, style: TextStyle(fontSize: width * 0.034))),
              )),
              DataCell(Center(
                child: (Text('-', textAlign: TextAlign.center, style: TextStyle(fontSize: width * 0.034))),
              )),
            ]),
            DataRow(cells: [
              DataCell(Center(
                child: (Text('Multimedia Programming',
                    textAlign: TextAlign.start, style: TextStyle(fontSize: width * 0.034))),
              )),
              DataCell(Center(
                child: (Text('600.00', textAlign: TextAlign.center, style: TextStyle(fontSize: width * 0.034))),
              )),
            ]),
            DataRow(cells: [
              DataCell(Center(
                child: (Text('Mobile Application Development',
                    textAlign: TextAlign.start, style: TextStyle(fontSize: width * 0.034))),
              )),
              DataCell(Center(
                child: (Text('600.00 ', textAlign: TextAlign.center, style: TextStyle(fontSize: width * 0.034))),
              )),
            ]),
            DataRow(cells: [
              DataCell(Center(
                child: (Text('NLP', textAlign: TextAlign.start, style: TextStyle(fontSize: width * 0.034))),
              )),
              DataCell(Center(
                child: (Text('300.00', textAlign: TextAlign.center, style: TextStyle(fontSize: width * 0.034))),
              )),
            ]),
            DataRow(cells: [
              DataCell(Center(
                child: (Text('Software Testing', textAlign: TextAlign.center, style: TextStyle(fontSize: width * 0.034))),
              )),
              DataCell(Center(
                child: (Text('600.00', textAlign: TextAlign.center, style: TextStyle(fontSize: width * 0.034))),
              )),
            ]),
            DataRow(cells: [
              DataCell(Center(
                child: (Text('OOAD', textAlign: TextAlign.start, style: TextStyle(fontSize: width * 0.034))),
              )),
              DataCell(Center(
                child: (Text('600.00', textAlign: TextAlign.center, style: TextStyle(fontSize: width * 0.034))),
              )),
            ]),
          ])
        ],
      ),
    ],
  );
}

Widget CardTotal() {
  return Card(
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
    color: Colors.white.withOpacity(0.87),
    child: SizedBox(
      height: 70,
      child: ListView(
        padding: EdgeInsets.all(10.0),
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "Total",
                style: TextStyle(
                    fontSize: 12,
                    color: Colors.amber.shade600,
                    fontWeight: FontWeight.normal),
              )
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("20,150.00",
                  style: TextStyle(
                      fontSize: 25,
                      color: Colors.amber.shade600,
                      height: 1.5,
                      fontWeight: FontWeight.bold))
            ],
          )
        ],
      ),
    ),
  );
}

import 'package:flutter/material.dart';

class Biography extends StatefulWidget {
  // const Biography({super.key});
  var orientation, size, height, width;

  @override
  State<Biography> createState() => _Biography();
}

class _Biography extends State<Biography> {
  @override
  Widget build(BuildContext context) {
    final orientation = MediaQuery.of(context).orientation;
    final size = MediaQuery.of(context).size;
    final height = size.height;
    final width = size.width;
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: buildAppBar(),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: NetworkImage(
                "https://scontent.fbkk5-7.fna.fbcdn.net/v/t1.15752-9/281152851_5910050635677276_1323122901884701339_n.png?_nc_cat=107&ccb=1-7&_nc_sid=ae9488&_nc_ohc=H4IUTTAn07AAX88N3aL&_nc_ht=scontent.fbkk5-7.fna&oh=03_AdQVJk8npvzUH1leIAcSFeIDo-AkkTGmuB-k64Y_wKFhvQ&oe=63F49539"),
            fit: BoxFit.cover,
          ),
        ),
        padding: EdgeInsets.all(15.0),
        child: ListView(
          children: <Widget>[
            ProfileBibliography(),
            CardProfile(width),
            CardOrganization(width),
            CardContact(width),
          ],
        ),
      ),
    );
  }

  AppBar buildAppBar() {
    return AppBar(
      backgroundColor: Colors.amberAccent.shade400,
      title: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            "BIOGRAPHY",
            style: TextStyle(color: Colors.black),
          )
        ],
      ),
    );
  }
}

Widget ProfileBibliography() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      Column(
        children: [
          Icon(Icons.account_circle, size: 150, color: Colors.amber),
        ],
      ),
    ],
  );
}

Widget CardProfile(width) {
  return Card(
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
    color: Colors.white,
    child: SizedBox(
      height: width * 0.47,
      child: ListView(
        padding: EdgeInsets.all(10.0),
        children: <Widget>[
          Row(
            children: [
              columnProfile(width),
              columnDataProfile(width),
            ],
          )
        ],
      ),
    ),
  );
}

Widget columnProfile(width) {
  return Padding(
    padding: EdgeInsets.only(left: 10),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text("PROFILE",
            style: TextStyle(
                height: 2.0,
                color: Colors.amberAccent.shade700,
                fontSize: width * 0.043)),
        Text("NAME :", style: TextStyle(height: 2.7, fontSize: width * 0.035)),
        Text("BIRTH DATE :",
            style: TextStyle(height: 2.0, fontSize: width * 0.035)),
        Text("GANDER :",
            style: TextStyle(height: 2.0, fontSize: width * 0.035)),
        Text("NATIONALITY :   ",
            style: TextStyle(height: 2.0, fontSize: width * 0.035)),
      ],
    ),
  );
}

Widget columnDataProfile(width) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Text("", style: TextStyle(height: 2.0, fontSize: width * 0.043)),
      Text("Natthakritta Nawachat",
          style: TextStyle(height: 2.7, fontSize: width * 0.035)),
      Text("25 JULY 2001",
          style: TextStyle(height: 2.0, fontSize: width * 0.035)),
      Text("Female", style: TextStyle(height: 2.0, fontSize: width * 0.035)),
      Text("Thai", style: TextStyle(height: 2.0, fontSize: width * 0.035)),
    ],
  );
}

Widget CardOrganization(width) {
  return Card(
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
    color: Colors.white,
    child: SizedBox(
      height: width * 0.68,
      child: ListView(
        padding: EdgeInsets.all(10.0),
        children: <Widget>[
          Row(
            children: <Widget>[
              columnOrganization(width),
              columnDataOrganization(width),
            ],
          )
        ],
      ),
    ),
  );
}

Widget columnOrganization(width) {
  return Padding(
    padding: EdgeInsets.only(left: 10),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text("BIOGRAPHY",
            style: TextStyle(
                height: 2.0,
                color: Colors.amberAccent.shade700,
                fontSize: width * 0.043)),
        Text("STUDENT ID :     ",
            style: TextStyle(height: 2.7, fontSize: width * 0.035)),
        Text("YEAR :", style: TextStyle(height: 2.0, fontSize: width * 0.035)),
        Text("FACULTY :",
            style: TextStyle(height: 2.0, fontSize: width * 0.035)),
        Text("PROGRAM :",
            style: TextStyle(height: 2.0, fontSize: width * 0.035)),
        Text("STATUS :",
            style: TextStyle(height: 2.0, fontSize: width * 0.035)),
        Text("ADVISROR :",
            style: TextStyle(height: 2.0, fontSize: width * 0.035)),
        Text("", style: TextStyle(height: 2.0, fontSize: width * 0.035)),
      ],
    ),
  );
}

Widget columnDataOrganization(width) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Text("", style: TextStyle(height: 2.0, fontSize: width * 0.043)),
      Text("63160195", style: TextStyle(height: 2.7, fontSize: width * 0.035)),
      Text("3", style: TextStyle(height: 2.0, fontSize: width * 0.035)),
      Text("Informatics",
          style: TextStyle(height: 2.0, fontSize: width * 0.035)),
      Text("2115020: Computer Science",
          style: TextStyle(height: 2.0, fontSize: width * 0.035)),
      Text("Studying", style: TextStyle(height: 2.0, fontSize: width * 0.035)),
      Text("MR.Pusit Kulkasem",
          style: TextStyle(height: 2.0, fontSize: width * 0.035)),
      Text("Asst. Prof. Dr.Komate Amphawan",
          style: TextStyle(height: 2.0, fontSize: width * 0.035)),
    ],
  );
}

Widget CardContact(width) {
  return Card(
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
    color: Colors.white,
    child: SizedBox(
      height: width * 0.41,
      child: ListView(
        padding: EdgeInsets.all(10.0),
        children: <Widget>[
          Row(
            children: [
              columnContact(width),
              columnDataContact(width),
            ],
          )
        ],
      ),
    ),
  );
}

Widget columnContact(width) {
  return Padding(
    padding: EdgeInsets.only(left: 10),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text("CONTACT INFO",
            style: TextStyle(
                height: 2.0,
                color: Colors.amberAccent.shade700,
                fontSize: width * 0.043)),
        Text("EMAIL :", style: TextStyle(height: 2.7, fontSize: width * 0.035)),
        Text("PHONE :", style: TextStyle(height: 2.0, fontSize: width * 0.035)),
        Text("ADDRESS :",
            style: TextStyle(height: 2.0, fontSize: width * 0.035)),
      ],
    ),
  );
}

Widget columnDataContact(width) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Text("", style: TextStyle(height: 2.0, fontSize: width * 0.043)),
      Text("63160195@go.buu.ac.th",
          style: TextStyle(height: 2.7, fontSize: width * 0.035)),
      Text("065-919-5566",
          style: TextStyle(height: 2.0, fontSize: width * 0.035)),
      Text("Bamnetnarong Chaiyaphum",
          style: TextStyle(height: 2.0, fontSize: width * 0.035)),
    ],
  );
}

import 'package:flutter/material.dart';

class Bio_Horizontal extends StatefulWidget {
  // const Biography({super.key});
  var orientation, size, height, width;

  @override
  State<Bio_Horizontal> createState() => _Bio_Horizontal();
}

class _Bio_Horizontal extends State<Bio_Horizontal> {
  @override
  Widget build(BuildContext context) {
    final orientation = MediaQuery.of(context).orientation;
    final size = MediaQuery.of(context).size;
    final height = size.height;
    final width = size.width;
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: buildAppBar(),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: NetworkImage(
                "https://scontent.fbkk5-7.fna.fbcdn.net/v/t1.15752-9/281152851_5910050635677276_1323122901884701339_n.png?_nc_cat=107&ccb=1-7&_nc_sid=ae9488&_nc_ohc=H4IUTTAn07AAX88N3aL&_nc_ht=scontent.fbkk5-7.fna&oh=03_AdQVJk8npvzUH1leIAcSFeIDo-AkkTGmuB-k64Y_wKFhvQ&oe=63F49539"),
            fit: BoxFit.cover,
          ),
        ),
        padding: EdgeInsets.all(15.0),
        child: ListView(
          children: <Widget>[
            ProfileBibliography(height),
            CardProfile(width, height),
            CardOrganization(width, height),
            CardContact(width, height),
          ],
        ),
      ),
    );
  }

  AppBar buildAppBar() {
    return AppBar(
      backgroundColor: Colors.amberAccent.shade400,
      title: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            "BIOGRAPHY",
            style: TextStyle(color: Colors.black),
          )
        ],
      ),
    );
  }
}

Widget ProfileBibliography(height) {
  return Row(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      Column(
        children: [
          Icon(Icons.account_circle, size: height * 0.2, color: Colors.amber),
        ],
      ),
    ],
  );
}

Widget CardProfile(width, height) {
  return Padding(
    padding: EdgeInsets.only(
        left: width * 0.1, right: width * 0.1, top: width * 0.01),
    child: Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
      color: Colors.white,
      child: SizedBox(
        height: height * 0.3,
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.only(left: width * 0.05, top: height * 0.01),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text("PROFILE",
                      style: TextStyle(
                          height: 2.0,
                          color: Colors.amberAccent.shade700,
                          fontSize: height * 0.04)),
                ],
              ),
            ),
            columnProfile(width, height)
          ],
        ),
      ),
    ),
  );
}

Widget columnProfile(width, height) {
  return Row(
    children: [
      Padding(
        padding: EdgeInsets.only(left: width * 0.05),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Column(
                  children: [
                    Text("NAME : ",
                        style: TextStyle(height: 2.7, fontSize: height * 0.03)),
                  ],
                ),
                Column(
                  children: [
                    Text("Natthakritta Nawachat",
                        style: TextStyle(height: 2.7, fontSize: height * 0.03)),
                  ],
                ),
              ],
            ),
            Row(
              children: [
                Column(
                  children: [
                    Text("GANDER : ",
                        style: TextStyle(height: 2.7, fontSize: height * 0.03)),
                  ],
                ),
                Column(
                  children: [
                    Text("Female",
                        style: TextStyle(height: 2.7, fontSize: height * 0.03)),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
      Padding(
        padding: EdgeInsets.only(left: width * 0.2),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Column(
                  children: [
                    Text("BIRTH DATE : ",
                        style: TextStyle(height: 2.7, fontSize: height * 0.03)),
                  ],
                ),
                Column(
                  children: [
                    Text("25 JULY 2001",
                        style: TextStyle(height: 2.7, fontSize: height * 0.03)),
                  ],
                ),
              ],
            ),
            Row(
              children: [
                Column(
                  children: [
                    Text("NATIONALITY : ",
                        style: TextStyle(height: 2.7, fontSize: height * 0.03)),
                  ],
                ),
                Column(
                  children: [
                    Text("Thai",
                        style: TextStyle(height: 2.7, fontSize: height * 0.03)),
                  ],
                ),
              ],
            ),
          ],
        ),
      )
    ],
  );
}

Widget CardOrganization(width,height) {
  return Padding(
    padding: EdgeInsets.only(
        left: width * 0.1, right: width * 0.1, top: width * 0.01),
    child: Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
      color: Colors.white,
      child: SizedBox(
        height: height * 0.45,
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.only(left: width * 0.05, top: height * 0.01),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text("BIOGRAPHY",
                      style: TextStyle(
                          height: 2.0,
                          color: Colors.amberAccent.shade700,
                          fontSize: height * 0.04)),
                ],
              ),
            ),
            columnOrganization(width, height),
            columnOrganization2(width, height),
          ],
        ),
      ),
    ),
  );
}

Widget columnOrganization(width, height) {
  return Row(
    children: [
      Padding(
        padding: EdgeInsets.only(left: width * 0.05),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Column(
                  children: [
                    Text("STUDENT ID : ",
                        style: TextStyle(height: 2.7, fontSize: height * 0.03)),
                  ],
                ),
                Column(
                  children: [
                    Text("63160195           ",
                        style: TextStyle(height: 2.7, fontSize: height * 0.03)),
                  ],
                ),
              ],
            ),
            Row(
              children: [
                Column(
                  children: [
                    Text("YEAR : ",
                        style: TextStyle(height: 2.7, fontSize: height * 0.03)),
                  ],
                ),
                Column(
                  children: [
                    Text("3",
                        style: TextStyle(height: 2.7, fontSize: height * 0.03)),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
      Padding(
        padding: EdgeInsets.only(left: width * 0.2),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Column(
                  children: [
                    Text("FACULTY : ",
                        style: TextStyle(height: 2.7, fontSize: height * 0.03)),
                  ],
                ),
                Column(
                  children: [
                    Text("Informatics",
                        style: TextStyle(height: 2.7, fontSize: height * 0.03)),
                  ],
                ),
              ],
            ),
            Row(
              children: [
                Column(
                  children: [
                    Text("STATUS : ",
                        style: TextStyle(height: 2.7, fontSize: height * 0.03)),
                  ],
                ),
                Column(
                  children: [
                    Text("Studying",
                        style: TextStyle(height: 2.7, fontSize: height * 0.03)),
                  ],
                ),
              ],
            ),
          ],
        ),
      )
    ],
  );
}

Widget columnOrganization2(width, height) {
  return Row(
    children: [
      Padding(
        padding: EdgeInsets.only(left: width * 0.05),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Column(
                  children: [
                    Text("PROGRAM : ",
                        style: TextStyle(height: 2.7, fontSize: height * 0.03)),
                  ],
                ),
                Column(
                  children: [
                    Text("2115020: Computer Science",
                        style: TextStyle(height: 2.7, fontSize: height * 0.03)),
                  ],
                ),
              ],
            ),
            Row(
              children: [
                Column(
                  children: [
                    Text("ADVISROR : ",
                        style: TextStyle(height: 2.7, fontSize: height * 0.03)),
                  ],
                ),
                Column(
                  children: [
                    Text("MR.Pusit Kulkasem, Asst. Prof. Dr.Komate Amphawan",
                        style: TextStyle(height: 2.7, fontSize: height * 0.03)),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    ],
  );
}

Widget CardContact(width,height) {
  return Padding(
    padding: EdgeInsets.only(
        left: width * 0.1, right: width * 0.1, top: width * 0.01),
    child: Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
      color: Colors.white,
      child: SizedBox(
        height: height * 0.3,
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.only(left: width * 0.05, top: height * 0.01),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Text("CONTACT INFO",
                      style: TextStyle(
                          height: 2.0,
                          color: Colors.amberAccent.shade700,
                          fontSize: height * 0.04)),
                ],
              ),
            ),
            columnContact(width, height),
            columnContact2(width, height),
          ],
        ),
      ),
    ),
  );
}

Widget columnContact(width, height) {
  return Row(
    children: [
      Padding(
        padding: EdgeInsets.only(left: width * 0.05),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Column(
                  children: [
                    Text("EMAIL : ",
                        style: TextStyle(height: 2.7, fontSize: height * 0.03)),
                  ],
                ),
                Column(
                  children: [
                    Text("63160195@go.buu.ac.th",
                        style: TextStyle(height: 2.7, fontSize: height * 0.03)),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
      Padding(
        padding: EdgeInsets.only(left: width * 0.2),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Column(
                  children: [
                    Text("PHONE : ",
                        style: TextStyle(height: 2.7, fontSize: height * 0.03)),
                  ],
                ),
                Column(
                  children: [
                    Text("069-999-9999",
                        style: TextStyle(height: 2.7, fontSize: height * 0.03)),
                  ],
                ),
              ],
            ),
          ],
        ),
      )
    ],
  );
}

Widget columnContact2(width, height) {
  return Row(
    children: [
      Padding(
        padding: EdgeInsets.only(left: width * 0.05),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Column(
                  children: [
                    Text("ADDRESS : ",
                        style: TextStyle(height: 2.7, fontSize: height * 0.03)),
                  ],
                ),
                Column(
                  children: [
                    Text("Bamnetnarong Chaiyaphum, Sansuk Bangsaen Chonburi",
                        style: TextStyle(height: 2.7, fontSize: height * 0.03)),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    ],
  );
}

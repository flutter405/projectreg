import 'package:flutter/material.dart';

void main() => runApp(MainTabletVertical());

class MainTabletVertical extends StatelessWidget {
  var orientation, size, height, width;

  @override
  Widget build(BuildContext context) {
    orientation = MediaQuery.of(context).orientation;
    size = MediaQuery.of(context).size;
    height = size.height;
    width = size.width;

    return Scaffold(
      extendBodyBehindAppBar: true,
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: NetworkImage(
                "https://scontent.fbkk5-7.fna.fbcdn.net/v/t1.15752-9/281152851_5910050635677276_1323122901884701339_n.png?_nc_cat=107&ccb=1-7&_nc_sid=ae9488&_nc_ohc=H4IUTTAn07AAX88N3aL&_nc_ht=scontent.fbkk5-7.fna&oh=03_AdQVJk8npvzUH1leIAcSFeIDo-AkkTGmuB-k64Y_wKFhvQ&oe=63F49539"),
            fit: BoxFit.cover,
          ),
        ),
        padding: EdgeInsets.only(top: 15, bottom: 15, left: 100, right: 100),
        child: ListView(
          children: [
            btnDay(width, height),
            StudyTimeTable(width, height),
            EnglishScore(width),
            newTopic(width),
          ],
        ),
      ),
    );
  }
}

Widget btnDay(width, height) {
  return Padding(
      padding: EdgeInsets.all(5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            children: <Widget>[
              SizedBox(
                height: height * 0.055,
                width: (width / 6) - 20,
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.0),
                    color: Colors.white,
                  ),
                  child: Center(
                    child:
                        Text('MON', style: TextStyle(fontSize: width * 0.03)),
                  ),
                ),
              )
            ],
          ),
          Column(
            children: <Widget>[
              SizedBox(
                height: height * 0.055,
                width: (width / 6) - 20,
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.0),
                    color: Colors.white,
                  ),
                  child: Center(
                    child:
                        Text('TUE', style: TextStyle(fontSize: width * 0.03)),
                  ),
                ),
              )
            ],
          ),
          Column(
            children: <Widget>[
              SizedBox(
                height: height * 0.055,
                width: (width / 6) - 20,
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.0),
                    color: Colors.grey[350],
                  ),
                  child: Center(
                    child:
                        Text('WED', style: TextStyle(fontSize: width * 0.03)),
                  ),
                ),
              )
            ],
          ),
          Column(
            children: <Widget>[
              SizedBox(
                height: height * 0.055,
                width: (width / 6) - 20,
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.0),
                    color: Colors.white,
                  ),
                  child: Center(
                    child:
                        Text('THU', style: TextStyle(fontSize: width * 0.03)),
                  ),
                ),
              )
            ],
          ),
          Column(
            children: <Widget>[
              SizedBox(
                height: height * 0.055,
                width: (width / 6) - 20,
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.0),
                    color: Colors.white,
                  ),
                  child: Center(
                    child:
                        Text('FRI', style: TextStyle(fontSize: width * 0.03)),
                  ),
                ),
              )
            ],
          ),
        ],
      ));
}

Widget StudyTimeTable(width, height) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.center,
    children: [
      Divider(height: 10, color: Colors.transparent),
      Row(
        children: <Widget>[
          Text(" Schedule",
              style: TextStyle(fontSize: width * 0.04, height: 1.5))
        ],
      ),
      Divider(height: 25),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Column(
            children: [
              Icon(
                Icons.account_circle,
                color: Colors.amberAccent.shade400,
                size: width * 0.15,
              )
            ],
          ),
          Column(
            children: [
              Card(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0)),
                  color: Colors.white.withOpacity(0.87),
                  child: Container(
                    padding: EdgeInsets.all(15.0),
                    height: width * 0.18,
                    width: width * 0.55,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Text('Mobile Application',
                                style: TextStyle(
                                    fontSize: width * 0.04,
                                    height: width * 0.0015))
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Column(
                              children: [
                                Text('IF-4C02',
                                    style: TextStyle(
                                        height: 2.0,
                                        color: Colors.grey,
                                        fontSize: width * 0.03))
                              ],
                            ),
                            Column(
                              children: [
                                Text('  10:00-12:00',
                                    style: TextStyle(
                                        height: 2.0,
                                        color: Colors.grey,
                                        fontSize: width * 0.03))
                              ],
                            )
                          ],
                        ),
                      ],
                    ),
                  ))
            ],
          )
        ],
      ),
      Divider(height: 15, color: Colors.transparent),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Column(
            children: [
              Icon(
                Icons.account_circle,
                color: Colors.amberAccent.shade400,
                size: width * 0.15,
              )
            ],
          ),
          Column(
            children: [
              Card(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0)),
                  color: Colors.white.withOpacity(0.87),
                  child: Container(
                    padding: EdgeInsets.all(15.0),
                    height: width * 0.18,
                    width: width * 0.55,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Text('Multimedia',
                                style: TextStyle(
                                    fontSize: width * 0.04,
                                    height: width * 0.0015))
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Column(
                              children: [
                                Text('IF-4C01',
                                    style: TextStyle(
                                        height: 2.0,
                                        color: Colors.grey,
                                        fontSize: width * 0.03))
                              ],
                            ),
                            Column(
                              children: [
                                Text('  13:00-16:00',
                                    style: TextStyle(
                                        height: 2.0,
                                        color: Colors.grey,
                                        fontSize: width * 0.03))
                              ],
                            )
                          ],
                        ),
                      ],
                    ),
                  ))
            ],
          )
        ],
      ),
      Divider(height: 15, color: Colors.transparent),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Column(
            children: [
              Icon(
                Icons.account_circle,
                color: Colors.amberAccent.shade400,
                size: width * 0.15,
              )
            ],
          ),
          Column(
            children: [
              Card(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0)),
                  color: Colors.white.withOpacity(0.87),
                  child: Container(
                    padding: EdgeInsets.all(15.0),
                    height: width * 0.18,
                    width: width * 0.55,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Text('Web Programming',
                                style: TextStyle(
                                    fontSize: width * 0.04,
                                    height: width * 0.0015))
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Column(
                              children: [
                                Text('IF-3C01',
                                    style: TextStyle(
                                        height: 2.0,
                                        color: Colors.grey,
                                        fontSize: width * 0.03))
                              ],
                            ),
                            Column(
                              children: [
                                Text('15:00-17:00',
                                    style: TextStyle(
                                        height: 2.0,
                                        color: Colors.grey,
                                        fontSize: width * 0.03))
                              ],
                            )
                          ],
                        ),
                      ],
                    ),
                  ))
            ],
          )
        ],
      ),
      Divider(height: 15, color: Colors.transparent),
    ],
  );
}

Widget EnglishScore(width) {
  return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
      color: Colors.white,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Padding(padding: EdgeInsets.only(left: 30),
          child: columnEnglishScore(width),)
        ],
      ));
}

Widget columnEnglishScore(width) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Text("ENGLISH SCORE",
          style: TextStyle(
              height: 2.5,
              color: Colors.amberAccent.shade700,
              fontSize: width * 0.035)),
      Text("*สำหรับนิสิตตั้งแต่รหัส59 เป็นต้นไปต้องสอบให้ครบ 2 ครั้ง",
          style: TextStyle(height: 2.5, fontSize: width * 0.025)),
      DataTable(columns: [
        DataColumn(
            label: Text('คะแนนสอบครั้งที่ 1',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: width * 0.025))),
        DataColumn(
            label: Text('คะแนนสอบครั้งที่ 2',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: width * 0.025))),
      ], rows: [
        DataRow(cells: [
          DataCell(Center(
            child: (Text('32',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: width * 0.025))),
          )),
          DataCell(Center(
            child: (Text('-',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: width * 0.025))),
          )),
        ]),
      ])
    ],
  );
}

Widget newTopic(width) {
  return Card(
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
    color: Colors.white,
    child: SizedBox(
      height: width * 0.55,
      child: ListView(
        scrollDirection: Axis.horizontal,
        padding: EdgeInsets.all(15.0),
        children: <Widget>[
          columnNewTopic(width),
          columnNewTopic2(width),
          // columnSecondExam(),
        ],
      ),
    ),
  );
}

Widget columnNewTopic(width) {
  return Center(
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: EdgeInsets.only(right: 50, left: 10),
          child: Text("NEWS TOPIC",
              style: TextStyle(
                  height: 2.0,
                  color: Colors.amberAccent.shade700,
                  fontSize: width * 0.035)),
        ),
        Padding(
          padding: EdgeInsets.only(right: 50, left: 10),
          child: Text("1. ประเมิณความคิดเห็นต่อสำนักงานอธิการบดี",
              style: TextStyle(height: 2.5, fontSize: width * 0.025)),
        ),
        pictureNewTopic(width),
      ],
    ),
  );
}

Widget pictureNewTopic(width) {
  return Column(
    children: [
      Padding(
          padding: EdgeInsets.only(top: 10, left: 30),
          child: Image.network(
              'https://scontent.fbkk5-7.fna.fbcdn.net/v/t1.15752-9/326509974_536050371838981_2831333027248372933_n.png?_nc_cat=107&ccb=1-7&_nc_sid=ae9488&_nc_eui2=AeG7Mqjdnib4E_xbJVLxlLXtl_e0pSphWeaX97SlKmFZ5jZlWZiuZ-7Kf-KueBIybdL_ioOzrpiA2ukqSHV7M3hL&_nc_ohc=apR9_-zzt1wAX8WCyQU&_nc_ht=scontent.fbkk5-7.fna&oh=03_AdTJfFxkRWXBKyP5ufS_gWtDtjOmcsNWgveCIO5mEY6ixA&oe=6422E91A',
              height: width * 0.35)),
    ],
  );
}

Widget columnNewTopic2(width) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Text(" ",
          style: TextStyle(
              height: 2.0,
              color: Colors.amberAccent.shade700,
              fontSize: width * 0.035)),
      Padding(
        padding: EdgeInsets.only(right: 50),
        child: Text("  2. การทำบัตรนิสิตกับธนาคารกรุงไทย",
            style: TextStyle(height: 2.5, fontSize: width * 0.025)),
      ),
      pictureNewTopic2(width),
    ],
  );
}

Widget pictureNewTopic2(width) {
  return Column(
    children: [
      Padding(
          padding: EdgeInsets.all(10),
          child: Image.network(
              'https://scontent.fbkk5-4.fna.fbcdn.net/v/t1.15752-9/326323529_859258131997513_5781641427720499130_n.png?_nc_cat=110&ccb=1-7&_nc_sid=ae9488&_nc_eui2=AeFCGNy8bSYcH-JhsLSlAOmY-NAC7ie5-2n40ALuJ7n7aYytC18uB_8UoIrjBj7vGxR91TryHQKFv29UkJC8VXio&_nc_ohc=L8MwzOfbk4cAX91lqGa&tn=ee5827Vjg-kg1k7B&_nc_ht=scontent.fbkk5-4.fna&oh=03_AdTQv1GMkKFl0QKrC9GRih1IodYnd1GKVj9JXB2wJ59ivg&oe=6422EBF6',
              height: width * 0.35)),
    ],
  );
}

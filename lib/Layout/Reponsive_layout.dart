import 'package:flutter/material.dart';

class ResponsiveLayout extends StatelessWidget {
  final Widget mobileBodyVertical;
  final Widget mobileBodyHorizontal;
  final Widget tabletBodyVertical;
  final Widget tabletBodyHorizontal;
  final Widget desktopBody;

  ResponsiveLayout(
      {required this.mobileBodyVertical,
      required this.mobileBodyHorizontal,
      required this.tabletBodyVertical,
      required this.tabletBodyHorizontal,
      required this.desktopBody});

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;
    return LayoutBuilder(
      builder: (context, constraints) {
        //mobile
        if((screenWidth>=350 && screenWidth<=480)&&
            (screenHeight>=630 && screenHeight<=970)){
        return mobileBodyVertical;
        }else if((screenWidth>= 630&& screenWidth<= 970)&&
            (screenHeight>=350 && screenHeight<=480)) {
          return mobileBodyHorizontal;
        }else if(screenHeight== 1880&& screenWidth== 1280){
          return tabletBodyVertical;
        }else if(screenHeight == 750 && screenWidth== 1620) {
          return desktopBody;
        }else if(screenHeight== 970&& screenWidth== 1800){
          return desktopBody;
        }else if((screenWidth>= 800&& screenWidth<= 1024)&&
            (screenHeight>=1080 && screenHeight<=1880)){
          return tabletBodyVertical;
        }else if((screenWidth>= 1080&& screenWidth<= 1880)&&
            (screenHeight>=800 && screenHeight<=1024)){
          return tabletBodyHorizontal;
        }
        else{
          return desktopBody;
        }
      },
    );
  }
}

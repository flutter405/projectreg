import 'package:flutter/material.dart';

import '../Desktop/Main_Desktop.dart';
import '../mobile/Main_Mobile_Vertical.dart';
import '../mobile/Main_Mobile_horizontal.dart';
import '../tablet/Main_Tablet_Vertical.dart';
import '../tablet/Main_Tablet_horizontal.dart';
import 'Reponsive_layout.dart';

class MainMobile extends StatelessWidget {
  const MainMobile({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ResponsiveLayout(
        mobileBodyVertical: MobileVertical(),
        mobileBodyHorizontal: MobileHorizontal(),
        tabletBodyVertical: MainTabletVertical(),
        tabletBodyHorizontal: MainTablet(),
        desktopBody: MainDesktop(),
      ),
    );
  }
}

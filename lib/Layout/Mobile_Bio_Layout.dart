import 'package:flutter/material.dart';

import '../mobile/Bio_Mobile_Horizontal.dart';
import '../mobile/Bio_Mobile_Vertical.dart';
import '../tablet/Bio_Tablet_Horizontal.dart';
import 'Reponsive_layout.dart';

class Mobile_Bio_Layout extends StatelessWidget {
  const Mobile_Bio_Layout({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // body: LayoutBuilder(
      //     builder: (BuildContext context, BoxConstraints constraints) {
      //       if(constraints.maxWidth < constraints.maxHeight){
      //         return Biography();
      //       }else{
      //         return Bio_Horizontal();
      //       }
      //     }),
      body: ResponsiveLayout(
        mobileBodyVertical: Biography(),
        mobileBodyHorizontal:  Bio_Horizontal(),
        tabletBodyVertical:  Biography(),
        tabletBodyHorizontal:  Bio_Tablet_Horizontal(),
        desktopBody:  Bio_Horizontal(),
      ),
    );
  }
}

import 'package:device_preview/device_preview.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'AlertDialog.dart';
import 'mobile/Bio_Mobile_Vertical.dart';
import 'EnrollmentResult.dart';
import 'ExamTable.dart';
import 'Graduation.dart';
import 'Layout/Mobile_Bio_Layout.dart';
import 'Scholarship.dart';
import 'Layout/Mobile_Main_Layout.dart';

void main() {
  runApp(DevicePreview(
      enabled: true,
      builder: (context) => MaterialApp(
          debugShowCheckedModeBanner: false,
          useInheritedMediaQuery: true,
          builder: DevicePreview.appBuilder,
          home: RegApp())));
}

class RegApp extends StatefulWidget {
  @override
  State<RegApp> createState() => _RegAppState();
}

class _RegAppState extends State<RegApp> {
  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.amberAccent.shade400,
        actions: [
          IconButton(
            onPressed: () {},
            icon: Icon(
              Icons.translate,
              color: Colors.white,
            ),
          ),
        ],
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "BURAPHA UNIVERSITY",
              style: TextStyle(color: Colors.black),
            )
          ],
        ),
      ),
      drawer: Drawer(
        child: ListView(
          children: <Widget>[
            DrawerHeader(
              child: Column(
                children: <Widget>[
                  Icon(Icons.account_circle, size: 100, color: Colors.amber),
                  Text('', style: TextStyle(fontSize: 10)),
                  Text('Natthakritta Nawachat')
                ],
              ),
            ),
            ListTile(
              hoverColor: Colors.amber.shade100,
              leading: Icon(Icons.person),
              title: const Text('Biography'),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Mobile_Bio_Layout()),
                );
              },
            ),
            ListTile(
              hoverColor: Colors.amber.shade100,
              leading: Icon(CupertinoIcons.doc),
              title: const Text('Enroll'),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => MyApp()),
                );
              },
            ),
            ListTile(
              hoverColor: Colors.amber.shade100,
              leading: Icon(CupertinoIcons.doc_checkmark),
              title: const Text('Enrollment Result'),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => EnrollmentResult()),
                );
              },
            ),
            ListTile(
              hoverColor: Colors.amber.shade100,
              leading: Icon(Icons.edit_calendar),
              title: const Text('Exam Timetable'),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => ExamTable()),
                );
              },
            ),
            ListTile(
              hoverColor: Colors.amber.shade100,
              leading: Icon(Icons.edit_note),
              title: const Text('Graduation check'),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Graduation()),
                );
              },
            ),
            ListTile(
              hoverColor: Colors.amber.shade100,
              leading: Icon(Icons.attach_money),
              title: const Text('Dept/scholarship'),
              onTap: () {
                Navigator.pop(context);
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Scholarship()),
                );
              },
            ),
            ListTile(
              hoverColor: Colors.amber.shade100,
              leading: Icon(
                Icons.power_settings_new,
                color: Colors.red,
              ),
              title: const Text('Logout',
                  style: TextStyle(
                    color: Colors.red,
                  )),
              onTap: () {
                Navigator.pop(context);
              },
            ),
          ],
        ),
      ),
      body: LayoutBuilder(
          builder: (BuildContext context, BoxConstraints constraints) {
        // if (constraints.maxWidth <480.0) {
        //   return MainSmallDevice();
        // } else {
        //   return _buildMaxContainer();
        // }
            return MainMobile();
      }),
    );
  }
}
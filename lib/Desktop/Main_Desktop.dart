import 'package:flutter/material.dart';

void main() => runApp(MainDesktop());

class MainDesktop extends StatelessWidget {
  var orientation, size, height, width;

  @override
  Widget build(BuildContext context) {
    orientation = MediaQuery.of(context).orientation;
    size = MediaQuery.of(context).size;
    height = size.height;
    width = size.width;

    return Scaffold(
      extendBodyBehindAppBar: true,
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: NetworkImage(
                "https://scontent.fbkk5-7.fna.fbcdn.net/v/t1.15752-9/281152851_5910050635677276_1323122901884701339_n.png?_nc_cat=107&ccb=1-7&_nc_sid=ae9488&_nc_ohc=H4IUTTAn07AAX88N3aL&_nc_ht=scontent.fbkk5-7.fna&oh=03_AdQVJk8npvzUH1leIAcSFeIDo-AkkTGmuB-k64Y_wKFhvQ&oe=63F49539"),
            fit: BoxFit.cover,
          ),
        ),
        padding: EdgeInsets.only(left: 40, top: 10, bottom: 10, right: 40),
        child: ListView(
          children: [
            Center(
              child: Row(
                children: [
                  Column(
                    children: [
                      btnDay(width, height),
                      StudyTimeTable(width, height),
                      EnglishScore(width),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      columnNewTopic(width),
                      columnNewTopic2(width),
                    ],
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

Widget btnDay(width, height) {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: [
      Padding(
        padding: EdgeInsets.only(left: 5, right: 5),
        child: Column(
          children: <Widget>[
            SizedBox(
              height: height * 0.08,
              width: (width / 9),
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  color: Colors.white,
                ),
                child: Center(
                  child: Text('MON', style: TextStyle(fontSize: width * 0.015)),
                ),
              ),
            )
          ],
        ),
      ),
      Padding(
        padding: EdgeInsets.only(left: 5, right: 5),
        child: Column(
          children: <Widget>[
            SizedBox(
              height: height * 0.08,
              width: (width / 9),
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  color: Colors.white,
                ),
                child: Center(
                  child: Text('TUE', style: TextStyle(fontSize: width * 0.015)),
                ),
              ),
            )
          ],
        ),
      ),
      Padding(
        padding: EdgeInsets.only(left: 5, right: 5),
        child: Column(
          children: <Widget>[
            SizedBox(
              height: height * 0.08,
              width: (width / 9),
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  color: Colors.grey[350],
                ),
                child: Center(
                  child: Text('WED', style: TextStyle(fontSize: width * 0.015)),
                ),
              ),
            )
          ],
        ),
      ),
      Padding(
        padding: EdgeInsets.only(left: 5, right: 5),
        child: Column(
          children: <Widget>[
            SizedBox(
              height: height * 0.08,
              width: (width / 9),
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  color: Colors.white,
                ),
                child: Center(
                  child: Text('THU', style: TextStyle(fontSize: width * 0.015)),
                ),
              ),
            )
          ],
        ),
      ),
      Padding(
        padding: EdgeInsets.only(left: 5, right: 50),
        child: Column(
          children: <Widget>[
            SizedBox(
              height: height * 0.08,
              width: (width / 9),
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10.0),
                  color: Colors.white,
                ),
                child: Center(
                  child: Text('FRI', style: TextStyle(fontSize: width * 0.015)),
                ),
              ),
            ),
          ],
        ),
      ),
    ],
  );
}

Widget StudyTimeTable(width, height) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Divider(height: 10, color: Colors.transparent),
      Row(
        children: <Widget>[
          Text(" Schedule",
              style: TextStyle(fontSize: width * 0.02, height: 1.5))
        ],
      ),
      Divider(height: 10),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Column(
            children: [
              Icon(
                Icons.account_circle,
                color: Colors.amberAccent.shade400,
                size: width * 0.09,
              )
            ],
          ),
          Column(
            children: [
              Card(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0)),
                  color: Colors.white.withOpacity(0.87),
                  child: Container(
                    padding: EdgeInsets.all(15.0),
                    height: height * 0.16,
                    width: width * 0.3,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Text('Mobile Application',
                                style: TextStyle(fontSize: height * 0.035))
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Column(
                              children: [
                                Text('IF-4C02',
                                    style: TextStyle(
                                        color: Colors.grey,
                                        fontSize: height * 0.03,
                                        height: height * 0.002))
                              ],
                            ),
                            Column(
                              children: [
                                Text('  10:00-12:00',
                                    style: TextStyle(
                                        color: Colors.grey,
                                        fontSize: height * 0.03,
                                        height: height * 0.002))
                              ],
                            )
                          ],
                        ),
                      ],
                    ),
                  ))
            ],
          )
        ],
      ),
      Divider(height: 15, color: Colors.transparent),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Column(
            children: [
              Icon(
                Icons.account_circle,
                color: Colors.amberAccent.shade400,
                size: width * 0.09,
              )
            ],
          ),
          Column(
            children: [
              Card(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0)),
                  color: Colors.white.withOpacity(0.87),
                  child: Container(
                    padding: EdgeInsets.all(15.0),
                    height: height * 0.16,
                    width: width * 0.3,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Text('Multimedia',
                                style: TextStyle(fontSize: height * 0.035))
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Column(
                              children: [
                                Text('IF-4C01',
                                    style: TextStyle(
                                        color: Colors.grey,
                                        fontSize: height * 0.03,
                                        height: height * 0.002))
                              ],
                            ),
                            Column(
                              children: [
                                Text('  13:00-16:00',
                                    style: TextStyle(
                                        color: Colors.grey,
                                        fontSize: height * 0.03,
                                        height: height * 0.002))
                              ],
                            )
                          ],
                        ),
                      ],
                    ),
                  ))
            ],
          )
        ],
      ),
      Divider(height: 15, color: Colors.transparent),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Column(
            children: [
              Icon(
                Icons.account_circle,
                color: Colors.amberAccent.shade400,
                size: width * 0.09,
              )
            ],
          ),
          Column(
            children: [
              Card(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0)),
                  color: Colors.white.withOpacity(0.87),
                  child: Container(
                    padding: EdgeInsets.all(15.0),
                    height: height * 0.16,
                    width: width * 0.3,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Text('Web Programming',
                                style: TextStyle(fontSize: height * 0.035))
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Column(
                              children: [
                                Text('IF-3C01',
                                    style: TextStyle(
                                        color: Colors.grey,
                                        fontSize: height * 0.03,
                                        height: height * 0.002))
                              ],
                            ),
                            Column(
                              children: [
                                Text('  15:00-17:00',
                                    style: TextStyle(
                                        color: Colors.grey,
                                        fontSize: height * 0.03,
                                        height: height * 0.002))
                              ],
                            )
                          ],
                        ),
                      ],
                    ),
                  ))
            ],
          )
        ],
      ),
      Divider(height: 15, color: Colors.transparent),
    ],
  );
}

Widget EnglishScore(width) {
  return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
      color: Colors.white,
      child: Padding(
        padding: EdgeInsets.only(
            top: 5, bottom: 5, left: width * 0.05, right: width * 0.05),
        child: Center(
          child: columnEnglishScore(width),
        ),
      ));
}

Widget columnEnglishScore(width) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Text("ENGLISH SCORE",
          style: TextStyle(
              height: 2.5,
              color: Colors.amberAccent.shade700,
              fontSize: width * 0.02)),
      Text("*สำหรับนิสิตตั้งแต่รหัส59 เป็นต้นไปต้องสอบให้ครบ 2 ครั้ง",
          style: TextStyle(height: 2.5, fontSize: width * 0.012)),
      DataTable(columns: [
        DataColumn(
            label: Text('คะแนนสอบครั้งที่ 1',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: width * 0.015))),
        DataColumn(
            label: Text('คะแนนสอบครั้งที่ 2',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: width * 0.015))),
      ], rows: [
        DataRow(cells: [
          DataCell(Center(
            child: (Text('32',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: width * 0.013))),
          )),
          DataCell(Center(
            child: (Text('-',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: width * 0.013))),
          )),
        ]),
      ])
    ],
  );
}

Widget columnNewTopic(width) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Padding(
        padding: EdgeInsets.only(right: 50, left: 10),
        child: Text("NEWS TOPIC",
            style: TextStyle(
                height: 2.0,
                color: Colors.amberAccent.shade700,
                fontSize: width * 0.02)),
      ),
      Padding(
        padding: EdgeInsets.only(right: 50, left: 10),
        child: Text("1.ประเมิณความคิดเห็นสำนักงานอธิการบดี",
            style: TextStyle(height: 2.5, fontSize: width * 0.015)),
      ),
      pictureNewTopic(width),
    ],
  );
}

Widget pictureNewTopic(width) {
  return Column(
    children: [
      Padding(
          padding: EdgeInsets.only(top: 10, left: 15),
          child: Image.network(
              'https://scontent.fbkk5-7.fna.fbcdn.net/v/t1.15752-9/326509974_536050371838981_2831333027248372933_n.png?_nc_cat=107&ccb=1-7&_nc_sid=ae9488&_nc_eui2=AeG7Mqjdnib4E_xbJVLxlLXtl_e0pSphWeaX97SlKmFZ5jZlWZiuZ-7Kf-KueBIybdL_ioOzrpiA2ukqSHV7M3hL&_nc_ohc=apR9_-zzt1wAX8WCyQU&_nc_ht=scontent.fbkk5-7.fna&oh=03_AdTJfFxkRWXBKyP5ufS_gWtDtjOmcsNWgveCIO5mEY6ixA&oe=6422E91A',
              width: width * 0.15)),
    ],
  );
}

Widget columnNewTopic2(width) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Text(" ",
          style: TextStyle(
              height: 2.0,
              color: Colors.amberAccent.shade700,
              fontSize: width * 0.02)),
      Padding(
        padding: EdgeInsets.only(right: 50),
        child: Text("    2.การทำบัตรนิสิตกับธนาคารกรุงไทย",
            style: TextStyle(height: 2.5, fontSize: width * 0.015)),
      ),
      pictureNewTopic2(width),
    ],
  );
}

Widget pictureNewTopic2(width) {
  return Column(
    children: [
      Padding(
          padding: EdgeInsets.only(top: 10, left: 15),
          child: Image.network(
              'https://scontent.fbkk5-4.fna.fbcdn.net/v/t1.15752-9/326323529_859258131997513_5781641427720499130_n.png?_nc_cat=110&ccb=1-7&_nc_sid=ae9488&_nc_eui2=AeFCGNy8bSYcH-JhsLSlAOmY-NAC7ie5-2n40ALuJ7n7aYytC18uB_8UoIrjBj7vGxR91TryHQKFv29UkJC8VXio&_nc_ohc=L8MwzOfbk4cAX91lqGa&tn=ee5827Vjg-kg1k7B&_nc_ht=scontent.fbkk5-4.fna&oh=03_AdTQv1GMkKFl0QKrC9GRih1IodYnd1GKVj9JXB2wJ59ivg&oe=6422EBF6',
              width: width * 0.15)),
    ],
  );
}

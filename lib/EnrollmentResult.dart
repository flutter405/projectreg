import 'package:flutter/material.dart';

class EnrollmentResult extends StatefulWidget {
  // const EnrollmentResult({super.key});
  var orientation, size, height, width;

  @override
  State<EnrollmentResult> createState() => _EnrollmentResult();
}

class _EnrollmentResult extends State<EnrollmentResult> {
  @override
  Widget build(BuildContext context) {
    final orientation = MediaQuery.of(context).orientation;
    final size = MediaQuery.of(context).size;
    final height = size.height;
    final width = size.width;

    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: buildAppBar(),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: NetworkImage(
                "https://scontent.fbkk5-7.fna.fbcdn.net/v/t1.15752-9/281152851_5910050635677276_1323122901884701339_n.png?_nc_cat=107&ccb=1-7&_nc_sid=ae9488&_nc_ohc=H4IUTTAn07AAX88N3aL&_nc_ht=scontent.fbkk5-7.fna&oh=03_AdQVJk8npvzUH1leIAcSFeIDo-AkkTGmuB-k64Y_wKFhvQ&oe=63F49539"),
            fit: BoxFit.cover,
          ),
        ),
        padding: EdgeInsets.all(15.0),
        child: ListView(
          children: <Widget>[
            Profile(width),
            Divider(height: 20),
            btnYear(width,height),
            Divider(height: 20),
            Semester5(width),
          ],
        ),
      ),
    );
  }

  AppBar buildAppBar() {
    return AppBar(
      backgroundColor: Colors.amberAccent.shade400,
      title: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            "ENROLLMENT RESULT",
            style: TextStyle(color: Colors.black),
          )
        ],
      ),
    );
  }
}

Widget Profile(width) {
  return Center(
    child: Row(
      children: <Widget>[
        Padding(padding: EdgeInsets.all(10),child: Column(
          children: [
            Icon(Icons.account_circle, size: width*0.27, color: Colors.amber),
          ],
        ),),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  'Student ID',
                  style: TextStyle(fontSize: width * 0.03, color: Colors.grey, height: 2.0),
                )
              ],
            ),
            Row(
              children: [
                Text('63160195', style: TextStyle(fontSize: width * 0.037, height: 1.5))
              ],
            ),
            Row(
              children: [
                Text('Name',
                    style:
                    TextStyle(fontSize: width * 0.03, color: Colors.grey, height: 2.0))
              ],
            ),
            Row(
              children: [
                Text('MISS NATTHAKRITTA NAWACHAT',
                    style: TextStyle(fontSize: width * 0.037, height: 1.5))
              ],
            ),
          ],
        ),
      ],
    ),
  );
}

Widget btnYear(width,height) {
  return Padding(
      padding: EdgeInsets.all(5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            children: <Widget>[
              SizedBox(
                height: height*0.07,
                width: (width/3)-18,
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.0),
                    color: Colors.white,
                  ),
                  child: Center(
                    child: Text('2020',style: TextStyle(fontSize: width * 0.05),),
                  ),
                ),
              )
            ],
          ),
          Column(
            children: <Widget>[
              SizedBox(
                height: height*0.07,
                width: (width/3)-18,
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.0),
                    color: Colors.white,
                  ),
                  child: Center(
                    child: Text('2021',style: TextStyle(fontSize: width * 0.05),),
                  ),
                ),
              )
            ],
          ),
          Column(
            children: <Widget>[
              SizedBox(
                height: height*0.07,
                width: (width/3)-18,
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10.0),
                    color: Colors.grey[350],
                  ),
                  child: Center(
                    child: Text('2022',style: TextStyle(fontSize: width * 0.05),),
                  ),
                ),
              )
            ],
          ),
        ],
      ));
}

Widget Semester5(width) {
  return Center(child: Column(
    crossAxisAlignment: CrossAxisAlignment.center,
    children: <Widget>[
      Row(
        // children: <Widget>[Text('Semester : 1/2022')],
        children: <Widget>[
          Column(
            children: [
              Text(
                'Semester : 1/2022',
                style: TextStyle(fontSize: width * 0.043),
              )
            ],
          ),
          Column(
            children: [
              Icon(Icons.arrow_drop_down,size: 30,)
            ],
          ),
        ],
      ),
      Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          DataTable(columns: [
            DataColumn(
                label: Text('Code',
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: width * 0.036))),
            DataColumn(
                label: Text('Course',
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: width * 0.036))),
            DataColumn(
                label: Text('Group',
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: width * 0.036))),
          ], rows: [
            DataRow(cells: [
              DataCell(Center(
                child: (Text('88636159', textAlign: TextAlign.center, style: TextStyle(fontSize: width * 0.036))),
              )),
              DataCell(Center(
                child: (Text('AI', textAlign: TextAlign.center, style: TextStyle(fontSize: width * 0.036))),
              )),
              DataCell(Center(
                child: (Text('2', textAlign: TextAlign.center, style: TextStyle(fontSize: width * 0.036))),
              )),
            ]),
            DataRow(cells: [
              DataCell(Center(
                child: (Text('88634159', textAlign: TextAlign.center, style: TextStyle(fontSize: width * 0.036))),
              )),
              DataCell(Center(
                child: (Text('Soft Dev', textAlign: TextAlign.center, style: TextStyle(fontSize: width * 0.036))),
              )),
              DataCell(Center(
                child: (Text('2', textAlign: TextAlign.center, style: TextStyle(fontSize: width * 0.036))),
              )),
            ]),
            DataRow(cells: [
              DataCell(Center(
                child: (Text('88624259', textAlign: TextAlign.center, style: TextStyle(fontSize: width * 0.036))),
              )),
              DataCell(Center(
                child: (Text('Mobile', textAlign: TextAlign.center, style: TextStyle(fontSize: width * 0.036))),
              )),
              DataCell(Center(
                child: (Text('2', textAlign: TextAlign.center, style: TextStyle(fontSize: width * 0.036))),
              )),
            ]),
            DataRow(cells: [
              DataCell(Center(
                child: (Text('88635359', textAlign: TextAlign.center, style: TextStyle(fontSize: width * 0.036))),
              )),
              DataCell(Center(
                child: (Text('UXUI', textAlign: TextAlign.center, style: TextStyle(fontSize: width * 0.036))),
              )),
              DataCell(Center(
                child: (Text('2', textAlign: TextAlign.center, style: TextStyle(fontSize: width * 0.036))),
              )),
            ]),
            DataRow(cells: [
              DataCell(Center(
                child: (Text('88633159', textAlign: TextAlign.center, style: TextStyle(fontSize: width * 0.036))),
              )),
              DataCell(Center(
                child: (Text('Com Net', textAlign: TextAlign.center, style: TextStyle(fontSize: width * 0.036))),
              )),
              DataCell(Center(
                child: (Text('2', textAlign: TextAlign.center, style: TextStyle(fontSize: width * 0.036))),
              )),
            ]),
            DataRow(cells: [
              DataCell(Center(
                child: (Text('88631159', textAlign: TextAlign.center, style: TextStyle(fontSize: width * 0.036))),
              )),
              DataCell(Center(
                child: (Text('Algorithm', textAlign: TextAlign.center, style: TextStyle(fontSize: width * 0.036))),
              )),
              DataCell(Center(
                child: (Text('2', textAlign: TextAlign.center, style: TextStyle(fontSize: width * 0.036))),
              )),
            ]),
          ])
        ],
      )
    ],
  ),);
}
